# MUSICBOX Installation Instructions

Scripts and documentation to setup and run Musicbox and FamilySong devices (raspberry pis)

You may want to use the 2017-02-21-template.img for installing a full-jessie pi. It has a cloned copy of this repository (slightly out of date) and most of the configuration done. You would still have to do:

- Review the autostart configuration as it depends on wether this is a musicbox or familysong.
- Install pitft or dac-amp.
- Change hostname, ssh keys, upload new ssh keys them to the tunnel, setup port forwarding on new port.

## Writing to SD card on Mac

https://www.raspberrypi.org/documentation/installation/installing-images/mac.md

Open a terminal, then run:

    diskutil list

Identify the disk (not partition) of your SD card e.g. disk4, not disk4s1.
Unmount your SD card by using the disk identifier, to prepare for copying data to it:

    diskutil unmountDisk /dev/disk<disk# from diskutil>

where disk is your BSD name e.g. diskutil unmountDisk /dev/disk4

Copy the data to your SD card:

    sudo dd bs=1M if=image.img of=/dev/rdisk<disk# from diskutil>

### Backup existing image

    dd if=/dev/rdisk<disk# from diskutil> of=image.img bs=1M

## Preparations before booting, on /boot

/boot is a FAT32 fs and thus edittable in any OS. Raspbian grabs many configuration files from this partition.

Run:

    ./pre_boot_wifi_ssh_setup.sh

Or do the following manually:

### Wifi
Copy wpa_supplicant.conf, from this repository, as necessary into /boot/wpa_supplicant.conf

### Enable SSH
Create an empty file on /boot/ssh to enable the ssh daemon and receive connections on port 22

    touch /Volumes/boot/ssh

## Boot

Configure basic stuff on

    sudo raspi-config

### Install basic packages
    sudo apt-get update -yq && sudo apt-get upgrade -yq && sudo apt-get install autossh mpc mpd git -yq

## Clone this repository
    git clone https://gitlab.com/jtibau/musicbox.git

### Autostart Configuration

Copy and configure files from autostart into ~/.config/autostart

### MPD

    mpc add http://musicbox.cs.vt.edu:PORT/mopidy


### Configuring Adafruit MAX98357 I2S Class-D Mono Amp

    curl -sS https://raw.githubusercontent.com/adafruit/Raspberry-Pi-Installer-Scripts/master/i2samp.sh | bash
    
### Pimoroni's pHAT DAC script

    curl -sS get.pimoroni.com/phatdac | bash   

### PiTFT 2.8 Capacitive

https://learn.adafruit.com/adafruit-2-8-pitft-capacitive-touch/easy-install

    curl -SLs https://apt.adafruit.com/add-pin | sudo bash
    sudo apt-get install -y raspberrypi-bootloader adafruit-pitft-helper raspberrypi-kernel

    sudo adafruit-pitft-helper -t 28c
    sudo reboot

To make sure the screen doesn't blank out with a screensaver, throw these lines at the bottom of ~/.config/lxsession/LXDE-pi/autostart

    @xset s noblank
    @xset s off
    @xset -dpms
